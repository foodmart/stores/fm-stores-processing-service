package fm.stores.processing.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import static fm.stores.processing.constant.StoreEntityConstants.Fields.CONTACT_FIELDS.STORE_FAX_FIELD_NAME;
import static fm.stores.processing.constant.StoreEntityConstants.Fields.CONTACT_FIELDS.STORE_PHONE_FIELD_NAME;

@Data
public class Contact {

    @Field(STORE_FAX_FIELD_NAME)
    private String storeFax;

    @Field(STORE_PHONE_FIELD_NAME)
    private String storePhone;

}
