package fm.stores.processing.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import static fm.stores.processing.constant.StoreEntityConstants.Fields.ADDRESS_FIELDS.*;

@Data
public class Address {

    @Field(STORE_CITY_FIELD_NAME)
    private String storeCity;

    @Field(STORE_COUNTRY_FIELD_NAME)
    private String storeCountry;

    @Field(STORE_MANAGER_FIELD_NAME)
    private String storeManager;

    @Field(STORE_POSTAL_CODE_FIELD_NAME)
    private String storePostalCode;

    @Field(STORE_STATE_FIELD_NAME)
    private String storeState;

    @Field(STORE_STREET_ADDRESS_FIELD_NAME)
    private String storeStreetAddress;
}
