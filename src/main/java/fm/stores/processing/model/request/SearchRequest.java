package fm.stores.processing.model.request;

import fm.stores.processing.model.type.LocalDateTimeType;
import fm.stores.processing.model.type.StringType;
import fm.stores.processing.model.type.Type;
import lombok.Data;


@Data
public class SearchRequest {

    private StringType storeName;

    private StringType storeType;

    private Type<Integer> storeNumber;

    private Type<Integer> regionId;

    private RegionSearchRequest region;

    private AddressSearchRequest address;

    private ContactSearchRequest contact;

    private Type<Boolean> coffeeBar;

    private Type<Boolean> florist;

    private Type<Boolean> preparedFood;

    private Type<Boolean> saladBar;

    private Type<Boolean> videoStore;

    private LocalDateTimeType firstOpenedDate;

    private LocalDateTimeType lastRemodelDate;

    private Type<Integer> frozenSQFT;

    private Type<Integer> grocerySQFT;

    private Type<Integer> meatSQFT;

    private Type<Integer> storeSQFT;
}
