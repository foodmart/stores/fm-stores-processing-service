package fm.stores.processing.model.request;

import fm.stores.processing.model.type.StringType;
import fm.stores.processing.model.type.Type;
import lombok.Data;

@Data
public class RegionSearchRequest {

    private StringType salesCity;

    private StringType salesCityProvince;

    private StringType salesDistrict;

    private StringType salesRegions;

    private StringType salesCountry;

    private Type<Integer> salesDistrictId;
}
