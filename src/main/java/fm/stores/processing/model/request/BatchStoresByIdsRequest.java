package fm.stores.processing.model.request;

import lombok.Data;

import java.util.List;

@Data
public class BatchStoresByIdsRequest {

    private List<Integer> storesIds;
}
