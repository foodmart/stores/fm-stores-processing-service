package fm.stores.processing.model.request;

import fm.stores.processing.model.type.StringType;
import lombok.Data;

@Data
public class ContactSearchRequest {

    private StringType storeFax;

    private StringType storePhone;

}
