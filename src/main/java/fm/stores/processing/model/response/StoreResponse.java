package fm.stores.processing.model.response;

import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.entity.Address;
import fm.stores.processing.model.entity.Contact;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

import static fm.stores.processing.constant.StoreEntityConstants.Fields.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StoreResponse {

    @Field(STORE_ID_FIELD_NAME)
    private int storeId;

    @Field(STORE_NAME_FIELD_NAME)
    private String storeName;

    @Field(STORE_TYPE_FILED_NAME)
    private String storeType;

    @Field(STORE_NUMBER_FIELD_NAME)
    private int storeNumber;

    @Field(REGION_ID_FIELD_NAME)
    private RegionResponse region;

    @Field(ADDRESS_FIELD_NAME)
    private Address address;

    @Field(CONTACT_FIELD_NAME)
    private Contact contact;

    @Field(COFFEE_BAR_FIELD_NAME)
    private boolean coffeeBar;

    @Field(FLORIST_FIELD_NAME)
    private boolean florist;

    @Field(PREPARED_FOOD_FIELD_NAME)
    private boolean preparedFood;

    @Field(SALAD_BAR_FIELD_NAME)
    private boolean saladBar;

    @Field(VIDEO_STORE_FIELD_NAME)
    private boolean videoStore;

    @Field(FIRST_OPENED_DATE_FIELD_NAME)
    private LocalDateTime firstOpenedDate;

    @Field(LAST_REMODE_DATE_FIELD_NAME)
    private LocalDateTime lastRemodelDate;

    @Field(FROZEN_SQFT_FILED_NAME)
    private int frozenSQFT;

    @Field(GROCERY_SQFT_FILED_NAME)
    private int grocerySQFT;

    @Field(MEAT_SQFT_FIELD_NAME)
    private int meatSQFT;

    @Field(STORE_SQFT_FIELD_NAME)
    private int storeSQFT;


}
