package fm.stores.processing.model.client.response;

import lombok.Data;

@Data
public class RegionResponse {

    private int regionId;

    private String salesCity;

    private String salesCityProvince;

    private String salesDistrict;

    private String salesRegions;

    private String salesCountry;

    private int salesDistrictId;
}
