package fm.stores.processing.constant;

public class StoreEntityConstants {

    public static final String COLLECTION_NAME = "store";

    public static class Fields {

        public static final String STORE_ID_FIELD_NAME = "store_id";
        public static final String STORE_NAME_FIELD_NAME = "store_name";
        public static final String STORE_TYPE_FILED_NAME = "store_type";
        public static final String STORE_NUMBER_FIELD_NAME = "store_number";
        public static final String REGION_ID_FIELD_NAME = "region_id";
        public static final String ADDRESS_FIELD_NAME = "address";

        public static class ADDRESS_FIELDS {
            public static final String STORE_CITY_FIELD_NAME = "store_city";
            public static final String STORE_COUNTRY_FIELD_NAME = "store_country";
            public static final String STORE_MANAGER_FIELD_NAME = "store_manager";
            public static final String STORE_POSTAL_CODE_FIELD_NAME = "store_postal_code";
            public static final String STORE_STATE_FIELD_NAME = "store_state";
            public static final String STORE_STREET_ADDRESS_FIELD_NAME = "store_street_address";
        }

        public static class CONTACT_FIELDS {
            public static final String STORE_FAX_FIELD_NAME = "store_fax";
            public static final String STORE_PHONE_FIELD_NAME = "store_phone";
        }

        public static final String CONTACT_FIELD_NAME = "contact";
        public static final String COFFEE_BAR_FIELD_NAME = "coffee_bar";
        public static final String FLORIST_FIELD_NAME = "florist";
        public static final String PREPARED_FOOD_FIELD_NAME = "prepared_food";
        public static final String SALAD_BAR_FIELD_NAME = "salad_bar";
        public static final String VIDEO_STORE_FIELD_NAME = "video_store";
        public static final String FIRST_OPENED_DATE_FIELD_NAME = "first_opened_date";
        public static final String LAST_REMODE_DATE_FIELD_NAME = "last_remodel_date";
        public static final String FROZEN_SQFT_FILED_NAME = "frozen_sqft";
        public static final String GROCERY_SQFT_FILED_NAME = "grocery_sqft";
        public static final String MEAT_SQFT_FIELD_NAME = "meat_sqft";
        public static final String STORE_SQFT_FIELD_NAME = "store_sqft";


    }
}
