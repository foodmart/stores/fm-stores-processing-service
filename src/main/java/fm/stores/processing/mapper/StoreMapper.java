package fm.stores.processing.mapper;

import fm.common.core.api.mapper.Mapper2;
import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.entity.Store;
import fm.stores.processing.model.response.StoreResponse;
import org.springframework.stereotype.Component;

@Component
public class StoreMapper implements Mapper2<Store, RegionResponse, StoreResponse> {
    @Override
    public StoreResponse map(Store store, RegionResponse regionResponse) {
        return StoreResponse.builder()
                .address(store.getAddress())
                .storeId(store.getStoreId())
                .contact(store.getContact())
                .florist(store.isFlorist())
                .coffeeBar(store.isCoffeeBar())
                .firstOpenedDate(store.getFirstOpenedDate())
                .grocerySQFT(store.getGrocerySQFT())
                .region(regionResponse)
                .storeName(store.getStoreName())
                .storeNumber(store.getStoreNumber())
                .storeSQFT(store.getStoreSQFT())
                .storeType(store.getStoreType())
                .videoStore(store.isVideoStore())
                .lastRemodelDate(store.getLastRemodelDate())
                .saladBar(store.isSaladBar())
                .meatSQFT(store.getMeatSQFT())
                .preparedFood(store.isPreparedFood())
                .frozenSQFT(store.getFrozenSQFT())
                .build();
    }

}
