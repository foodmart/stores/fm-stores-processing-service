package fm.stores.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.stores.processing.model.response.StoreResponse;
import fm.stores.processing.model.response.StoresResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StoresMapper implements Mapper<List<StoreResponse>, StoresResponse> {
    @Override
    public StoresResponse map(List<StoreResponse> storeResponses) {
        return StoresResponse.builder()
                .stores(storeResponses)
                .build();
    }
}
