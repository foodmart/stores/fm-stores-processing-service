package fm.stores.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.stores.processing.model.response.StoresIdsResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StoresIdsResponseMapper implements Mapper<List<Integer>, StoresIdsResponse> {
    @Override
    public StoresIdsResponse map(List<Integer> integers) {
        return StoresIdsResponse.builder()
                .storesIds(integers)
                .build();
    }
}
