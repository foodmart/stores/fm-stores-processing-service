package fm.stores.processing.repository.impl;

import fm.stores.processing.model.entity.Store;
import fm.stores.processing.repository.api.StoresRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static fm.stores.processing.constant.StoreEntityConstants.Fields.STORE_ID_FIELD_NAME;

@Repository
@RequiredArgsConstructor
public class StoresRepositoryImpl implements StoresRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public Optional<Store> findStoreById(int storeId) {
        Criteria criteria = Criteria.where(STORE_ID_FIELD_NAME).is(storeId);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Store.class).stream().findFirst();
    }

    @Override
    public List<Store> findStoresByIds(List<Integer> storesIds) {
        Criteria criteria = Criteria.where(STORE_ID_FIELD_NAME).in(storesIds);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Store.class);
    }

    @Override
    public List<Store> findStores(Query query) {
        return mongoTemplate.find(query, Store.class);
    }

    @Override
    public List<Store> findStores(Query query, int page, int size) {
        query = query.with(PageRequest.of(page, size));
        return mongoTemplate.find(query, Store.class);
    }
}
