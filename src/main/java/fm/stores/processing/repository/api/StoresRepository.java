package fm.stores.processing.repository.api;

import fm.stores.processing.model.entity.Store;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Optional;

public interface StoresRepository {

    Optional<Store> findStoreById(int storeId);

    List<Store> findStoresByIds(List<Integer> storesIds);

    List<Store> findStores(Query query);

    List<Store> findStores(Query query, int page, int size);
}
