package fm.stores.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.stores.processing.model.request.BatchStoresByIdsRequest;
import fm.stores.processing.model.request.SearchRequest;
import fm.stores.processing.model.response.StoreResponse;
import fm.stores.processing.model.response.StoresIdsResponse;
import fm.stores.processing.model.response.StoresResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface StoresDocs {

    @GetMapping("/{store_id}")
    @CommonParams
    ResponseCoreEntity<StoreResponse> findStoreById(@PathVariable("store_id") int storeId);

    @PostMapping("/batch")
    @CommonParams
    ResponseCoreEntity<StoresResponse> findStoresByIds(@RequestBody BatchStoresByIdsRequest request);

    @PostMapping("/search")
    @CommonParams
    @Pageable
    ResponseCoreEntity<StoresResponse> findStores(@RequestBody SearchRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseCoreEntity<StoresIdsResponse> findStoresIds(@RequestBody SearchRequest request);
}
