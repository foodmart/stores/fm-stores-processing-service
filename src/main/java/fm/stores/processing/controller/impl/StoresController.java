package fm.stores.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.stores.processing.controller.doc.StoresDocs;
import fm.stores.processing.model.request.BatchStoresByIdsRequest;
import fm.stores.processing.model.request.SearchRequest;
import fm.stores.processing.model.response.StoreResponse;
import fm.stores.processing.model.response.StoresIdsResponse;
import fm.stores.processing.model.response.StoresResponse;
import fm.stores.processing.service.api.StoresService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StoresController extends BaseController implements StoresDocs {

    private final StoresService storesService;

    @Override
    public ResponseCoreEntity<StoreResponse> findStoreById(int storeId) {
        return response(storesService.findStoreById(storeId));
    }

    @Override
    public ResponseCoreEntity<StoresResponse> findStoresByIds(BatchStoresByIdsRequest request) {
        return response(storesService.findStoresByIds(request.getStoresIds()));
    }

    @Override
    public ResponseCoreEntity<StoresResponse> findStores(SearchRequest request) {
        return response(storesService.findStores(request));
    }

    @Override
    public ResponseCoreEntity<StoresIdsResponse> findStoresIds(SearchRequest request) {
        return response(storesService.findStoresIds(request));
    }
}
