package fm.stores.processing.client;


import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.stores.processing.model.client.request.BatchRegionsByIdsRequest;
import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.client.response.RegionsIdsResponse;
import fm.stores.processing.model.client.response.RegionsResponse;
import fm.stores.processing.model.request.RegionSearchRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "fm-regions-processing-service", url = "${config.feign.fm-regions-processing-service}")
public interface RegionsClient {

    @GetMapping("/{region_id}")
    @CommonParams
    ResponseBodyEntity<RegionResponse> findRegionById(@PathVariable("region_id") int regionId);

    @PostMapping("/batch")
    @CommonParams
    ResponseBodyEntity<RegionsResponse> findRegionsByIds(@RequestBody BatchRegionsByIdsRequest request);

    @PostMapping("/search/ids")
    @CommonParams
    ResponseBodyEntity<RegionsIdsResponse> findRegionsIds(@RequestBody RegionSearchRequest request);
}
