package fm.stores.processing.service.api;

import fm.stores.processing.model.request.SearchRequest;
import org.springframework.data.mongodb.core.query.Query;

public interface QueryBuilder {

    Query build(SearchRequest request);
}
