package fm.stores.processing.service.api;


import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.client.response.RegionsResponse;
import fm.stores.processing.model.request.RegionSearchRequest;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface RegionsService {

    RegionResponse findRegionById(int regionId);

    RegionsResponse findRegionsByIds(List<Integer> regionsIds);
    List<Integer> findRegionsIds(@RequestBody RegionSearchRequest request);

}
