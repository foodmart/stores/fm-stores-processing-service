package fm.stores.processing.service.api;

import fm.stores.processing.model.request.SearchRequest;
import fm.stores.processing.model.response.StoreResponse;
import fm.stores.processing.model.response.StoresIdsResponse;
import fm.stores.processing.model.response.StoresResponse;

import java.util.List;

public interface StoresService {

    StoreResponse findStoreById(int storeId);

    StoresResponse findStoresByIds(List<Integer> storesIds);

    StoresResponse findStores(SearchRequest request);

    StoresIdsResponse findStoresIds(SearchRequest request);
}
