package fm.stores.processing.service.impl;

import fm.common.core.api.mapper.MapperProvider;
import fm.common.core.model.header.CommonContext;
import fm.stores.processing.exception.StoreNotFoundException;
import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.client.response.RegionsResponse;
import fm.stores.processing.model.entity.Store;
import fm.stores.processing.model.request.SearchRequest;
import fm.stores.processing.model.response.StoreResponse;
import fm.stores.processing.model.response.StoresIdsResponse;
import fm.stores.processing.model.response.StoresResponse;
import fm.stores.processing.repository.api.StoresRepository;
import fm.stores.processing.service.api.QueryBuilder;
import fm.stores.processing.service.api.RegionsService;
import fm.stores.processing.service.api.StoresService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StoresServiceImpl implements StoresService {

    private final StoresRepository storesRepository;

    private final RegionsService regionsService;

    private final QueryBuilder queryBuilder;

    private final MapperProvider mapperProvider;

    private final CommonContext commonContext;

    @Override
    public StoreResponse findStoreById(int storeId) {
        final Optional<Store> storeOpt = storesRepository.findStoreById(storeId);
        if (storeOpt.isPresent()) {
            RegionResponse region = regionsService.findRegionById(storeOpt.get().getRegionId());
            return mapperProvider.map(storeOpt.get(), region, StoreResponse.class);
        }
        throw new StoreNotFoundException();
    }

    @Override
    public StoresResponse findStoresByIds(List<Integer> storesIds) {
        List<Store> stores = storesRepository.findStoresByIds(storesIds);
        List<StoreResponse> storeResponses = storesCollector(stores);
        return mapperProvider.map(storeResponses, StoresResponse.class);
    }

    @Override
    public StoresResponse findStores(SearchRequest request) {
        List<Store> stores = storesRepository.findStores(queryBuilder.build(request), commonContext.getPage(), commonContext.getSize());
        return mapperProvider.map(storesCollector(stores), StoresResponse.class);
    }

    @Override
    public StoresIdsResponse findStoresIds(SearchRequest request) {
        return mapperProvider.map(storesRepository.findStores(queryBuilder.build(request))
                        .stream()
                        .map(Store::getStoreId)
                        .toList()
                , StoresIdsResponse.class);
    }

    private List<StoreResponse> storesCollector(List<Store> stores) {
        RegionsResponse regions = regionsService.findRegionsByIds(stores.stream().map(Store::getRegionId).toList());
        return stores.stream().map(store -> {
            RegionResponse region = regions.getRegions()
                    .stream()
                    .filter(regionResponse -> regionResponse.getRegionId() == store.getRegionId())
                    .findFirst()
                    .orElse(null);
            return mapperProvider.map(store, region, StoreResponse.class);
        }).toList();
    }
}
