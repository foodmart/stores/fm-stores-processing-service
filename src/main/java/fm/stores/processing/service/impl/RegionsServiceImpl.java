package fm.stores.processing.service.impl;

import fm.stores.processing.client.RegionsClient;
import fm.stores.processing.model.client.request.BatchRegionsByIdsRequest;
import fm.stores.processing.model.client.response.RegionResponse;
import fm.stores.processing.model.client.response.RegionsResponse;
import fm.stores.processing.model.request.RegionSearchRequest;
import fm.stores.processing.service.api.RegionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RegionsServiceImpl implements RegionsService {

    private final RegionsClient regionsClient;

    @Override
    public RegionResponse findRegionById(int regionId) {
        return regionsClient.findRegionById(regionId)
                .getData();
    }

    @Override
    public RegionsResponse findRegionsByIds(List<Integer> regionsIds) {
        return regionsClient.findRegionsByIds(BatchRegionsByIdsRequest.builder()
                        .regionsIds(regionsIds)
                        .build())
                .getData();
    }

    @Override
    public List<Integer> findRegionsIds(RegionSearchRequest request) {
        return regionsClient.findRegionsIds(request)
                .getData()
                .getRegionsIds();
    }
}
