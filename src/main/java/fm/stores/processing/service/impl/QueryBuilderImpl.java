package fm.stores.processing.service.impl;

import fm.stores.processing.model.request.AddressSearchRequest;
import fm.stores.processing.model.request.ContactSearchRequest;
import fm.stores.processing.model.request.SearchRequest;
import fm.stores.processing.model.type.LocalDateTimeType;
import fm.stores.processing.model.type.StringType;
import fm.stores.processing.model.type.Type;
import fm.stores.processing.service.api.QueryBuilder;
import fm.stores.processing.service.api.RegionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import static fm.stores.processing.constant.StoreEntityConstants.Fields.ADDRESS_FIELDS.*;
import static fm.stores.processing.constant.StoreEntityConstants.Fields.*;
import static fm.stores.processing.constant.StoreEntityConstants.Fields.CONTACT_FIELDS.STORE_FAX_FIELD_NAME;
import static fm.stores.processing.constant.StoreEntityConstants.Fields.CONTACT_FIELDS.STORE_PHONE_FIELD_NAME;

@Service
@RequiredArgsConstructor
public class QueryBuilderImpl implements QueryBuilder {

    private static final String JOIN_SYMBOL = ".";

    private final RegionsService regionsService;

    @Override
    public Query build(SearchRequest request) {
        Criteria criteria = new Criteria();
        List<Integer> regionsIds = new ArrayList<>();
        if (request.getRegion() != null) {
            regionsIds.addAll(regionsService.findRegionsIds(request.getRegion()));
        }
        addSearchRequestId(regionsIds, request.getRegionId());
        addStoreCriteria(criteria, request);
        if ((request.getRegionId() != null) || request.getRegion() != null) {
            criteria.and(REGION_ID_FIELD_NAME).in(regionsIds);
        }
        return Query.query(criteria);
    }

    private void addStoreCriteria(Criteria criteria, SearchRequest request) {
        addTypeCriteria(criteria, request.getStoreName(), STORE_NAME_FIELD_NAME);
        addTypeCriteria(criteria, request.getStoreType(), STORE_TYPE_FILED_NAME);
        addTypeCriteria(criteria, request.getStoreNumber(), STORE_NUMBER_FIELD_NAME);
        addTypeCriteria(criteria, request.getCoffeeBar(), COFFEE_BAR_FIELD_NAME);
        addTypeCriteria(criteria, request.getFlorist(), FLORIST_FIELD_NAME);
        addTypeCriteria(criteria, request.getPreparedFood(), PREPARED_FOOD_FIELD_NAME);
        addTypeCriteria(criteria, request.getSaladBar(), SALAD_BAR_FIELD_NAME);
        addTypeCriteria(criteria, request.getVideoStore(), VIDEO_STORE_FIELD_NAME);
        addTypeCriteria(criteria, request.getFirstOpenedDate(), FIRST_OPENED_DATE_FIELD_NAME);
        addTypeCriteria(criteria, request.getLastRemodelDate(), FROZEN_SQFT_FILED_NAME);
        addTypeCriteria(criteria, request.getLastRemodelDate(), LAST_REMODE_DATE_FIELD_NAME);
        addTypeCriteria(criteria, request.getLastRemodelDate(), GROCERY_SQFT_FILED_NAME);
        addTypeCriteria(criteria, request.getLastRemodelDate(), MEAT_SQFT_FIELD_NAME);
        addTypeCriteria(criteria, request.getLastRemodelDate(), STORE_SQFT_FIELD_NAME);
        if (request.getContact() != null) {
            addContactCriteria(criteria, request.getContact());
        }
        if (request.getAddress() != null) {
            addAddressCriteria(criteria, request.getAddress());
        }
    }

    private void addContactCriteria(Criteria criteria, ContactSearchRequest request) {
        addTypeCriteria(criteria, request.getStoreFax(), CONTACT_FIELD_NAME + JOIN_SYMBOL + STORE_FAX_FIELD_NAME);
        addTypeCriteria(criteria, request.getStorePhone(), CONTACT_FIELD_NAME + JOIN_SYMBOL + STORE_PHONE_FIELD_NAME);
    }

    private void addAddressCriteria(Criteria criteria, AddressSearchRequest request) {
        addTypeCriteria(criteria, request.getStoreCity(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_CITY_FIELD_NAME);
        addTypeCriteria(criteria, request.getStoreCountry(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_COUNTRY_FIELD_NAME);
        addTypeCriteria(criteria, request.getStoreManager(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_MANAGER_FIELD_NAME);
        addTypeCriteria(criteria, request.getStorePostalCode(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_POSTAL_CODE_FIELD_NAME);
        addTypeCriteria(criteria, request.getStoreState(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_STATE_FIELD_NAME);
        addTypeCriteria(criteria, request.getStoreStreetAddress(), ADDRESS_FIELD_NAME + JOIN_SYMBOL + STORE_STREET_ADDRESS_FIELD_NAME);
    }

    private void addTypeCriteria(Criteria criteria, Type<?> type, String fieldName) {
        if (type != null) {
            if (type instanceof StringType stringType) {
                if (stringType.isExact()) {
                    criteria.and(fieldName).is(stringType.getValue());
                } else {
                    criteria.and(fieldName).regex(stringType.getValue());
                }
            } else if (type instanceof LocalDateTimeType dateTimeType) {
                criteria.and(fieldName).is(dateTimeType.getValue().atZone(ZoneOffset.systemDefault()).toInstant());
            } else {
                criteria.and(fieldName).is(type.getValue());
            }
        }
    }

    private <T> void addSearchRequestId(List<T> ids, Type<T> type) {
        if (type != null) {
            ids.add(type.getValue());
        }
    }
}
